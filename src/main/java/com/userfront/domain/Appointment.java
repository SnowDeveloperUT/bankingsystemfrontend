package com.userfront.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.jws.soap.SOAPBinding;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by snowwhite on 7/12/2017.
 */
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
@Entity
public class Appointment {
	@Id
	@GeneratedValue
	private Long id;
	private Date date;
	private String location;
	private String description;
	private boolean confirmed;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
}
