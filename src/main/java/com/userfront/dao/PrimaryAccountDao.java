package com.userfront.dao;

import com.userfront.domain.PrimaryAccount;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by snowwhite on 7/16/2017.
 */
public interface PrimaryAccountDao extends CrudRepository<PrimaryAccount, Long> {
	PrimaryAccount findByAccountNumber (int accountNumber);
}
