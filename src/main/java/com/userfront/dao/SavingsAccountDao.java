package com.userfront.dao;

import com.userfront.domain.SavingsAccount;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by snowwhite on 7/16/2017.
 */
public interface SavingsAccountDao extends CrudRepository<SavingsAccount,Long> {
	SavingsAccount findByAccountNumber (int accountNumber);
}
